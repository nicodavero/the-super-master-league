import React from "react";
import "./css/Team.css";

function Team({name, logo, credits, players}){
    return(
        <div className="teams-wrapper">
            <div className="team-row">
                <img className="logo" src={logo} alt="" />
                <h4>{name}</h4>
                <span>
                    <h5>Crediti</h5>
                    <h3>{credits}</h3>
                </span>
            </div>
            {players.map(player => (
                <div className="player-wrapper">
                    <p>{player.name}</p>
                    <p className="p-blue">{player.price}</p>
                </div>
            ))}
        </div>
    );
};

export default Team;
