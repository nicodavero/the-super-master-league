import React from "react";
import "./css/Header.css";

function Header(){
    return(
        <div className="header">
            <img src="./media/logo_transparent.png" alt="" />
        </div>
    )
}

export default Header;